$(document).on("click", ".caption_nav_prev a", function ()
{
    var date = $(this).attr('id').substr(2);
    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
});

$(document).on("click", ".caption_nav_next a", function ()
{
    var date = $(this).attr('id').substr(2);
    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
});

$(document).ready(function () {
    
    $('#btnFilePos').on('click', function() {
        $('#avatar_f').trigger('click');
    });


    var isMobileView = false;
    var isTabletView = false;
    var isModuleHeightAlign = false;
    var asideHeight;
    
    updateUI();
    // resizeModule();
    
    $('input[type="submit"]').wrap('<div class="base--button"></div>');
    $('#mod_login .button, #mod_forum .button, #mod_questionnaire .buttonWrapper .button').wrap('<div class="base--button"></div>');
    $('.button-svg').wrap('<div class="base--button"></div>')
    $('#editForm #btnFilePos').wrap('<div class="base--button"></div>');
    $('#registerForm .btnForm, #loginForm .btnForm, #formAddPost .btnForm, #addForm .btnForm, #delForm .btnForm, #formAddJoke .btnForm').wrap('<div class="base--button"></div>');
    $('#goTop').on('click', function(e) {
        e.preventDefault();
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        return false; 
    });

    function textSize() {
        var textHero = $('.hero__info').text().toString();
        if (textHero.length > 45) {
            $('.hero__info').addClass('font--small');
        }
    }

    textSize();

    function equalizeHeights(selector) {
        var heights = new Array();
        $(selector).each(function() {
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            heights.push($(this).height());
        });

        var max = Math.max.apply( Math, heights );

        $(selector).each(function() {
            $(this).css('height', max + 'px');
        }); 
    }

    function equalizeHeightsWithPadding(selector) {
        var heights = new Array();
        $(selector).each(function() {
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            heights.push($(this).outerHeight());
        });

        var max = Math.max.apply( Math, heights );

        $(selector).each(function() {
            $(this).css('height', max + 'px');
        }); 
    }

    countModules();

    $(window).load(function() {
        equalizeHeights('.headmodules__list h2');
        
        var iv = null;
        $(window).resize(function() {
            if (Modernizr.mq('(min-width: 992px)')) {
                equalizeHeights('.aside, .main');
            }
            countModules();
            if(iv !== null) {
                window.clearTimeout(iv);
            }

            iv = setTimeout(function() {
                    equalizeHeights('.headmodules__list h2');
            }, 120);
        });

        if (Modernizr.mq('(min-width: 992px)')) {
            equalizeHeights('.aside, .main');
        }
    });

     if (($('.headmodules__list > li').length) > 1) {
        equalizeHeights();        
    }

    function watch() {
        var s = Snap(document.getElementById("clock"));
        var seconds = s.select("#handSecond"),
            minutes = s.select("#handMinute"),
            hours = s.select("#handHour"),
            face = {
                elem: s.select("#face"),
                cx: s.select("#face").getBBox().cx,
                cy: s.select("#face").getBBox().cy
            },
            angle = 0;
        function update() {
            var time = new Date();
            setHours(time);
            setMinutes(time);
            setSeconds(time);
        }
        function setHours(t) {
            var hour = t.getHours();
            hour %= 12;
            hour += Math.floor(t.getMinutes()/10)/6;
            var angle = hour*360/12;
            hours.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                100,
                mina.linear,
                function(){
                    if (angle === 360) {
                        hours.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});               
                    }
                }
              );
        }
        function setMinutes(t) {
            var minute = t.getMinutes();
            minute %= 60;
            minute += Math.floor(t.getSeconds()/10)/6;
            var angle = minute*360/60;
            minutes.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                100,
                mina.linear,
                function() {
                    if (angle === 360) {
                        minutes.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});
                    }
                }
            );
        }
        function setSeconds(t) {
            t = t.getSeconds();
            t %= 60;
            var angle = t*360/60;
            if (angle === 0) angle = 360;
            seconds.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                600,
                mina.elastic,
                function(){
                    if (angle === 360) {
                        seconds.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});     
                    }
                }
            );
        }
        update();
        setInterval(update, 1000);
    }

    if ($('#face').length > 0) {
        watch();
    }
    

    function countModules() {
        switch($('.modules-top__list > li').length) {
            case 1:
                $('.modules-top__list > li').addClass('one');
                break;
            case 2:
                $('.modules-top__list > li').addClass('two');
                break;
            case 3:
               $('.modules-top__list > li').addClass('three');
                break;
        }

        switch($('#modules-top2 > ul > li').length) {
            case 1:
                $('#modules-top2 > ul > li').addClass('one');
                break;
            case 2:
                $('#modules-top2 > ul > li').addClass('two');
                break;
            case 3:
               $('#modules-top2 > ul > li').addClass('three');
                break;
        }

        switch($('#modules-bottom > ul > li').length) {
            case 1:
                $('#modules-bottom > ul > li').addClass('one');
                break;
            case 2:
                $('#modules-bottom > ul > li').addClass('two');
                break;
            case 3:
               $('#modules-bottom > ul > li').addClass('three');
                break;  
        }
    }

    function modulesContent() {
        var one = $('.modules-content > ul > .one > a > .modules-content__background');
        var oneFilter = $('.modules-content > ul > .one > a > .modules-content__background filter');
        var oneTextureOne = $('.modules-content > ul > .one > a > .modules-content__background .mc-texture-1');
        var oneTextureTwo = $('.modules-content > ul > .one > a > .modules-content__background .mc-texture-2');
        var onePathOne = $('.modules-content > ul > .one > a > .modules-content__background .one-path-1');
        var onePathTwo = $('.modules-content > ul > .one > a > .modules-content__background .one-path-2');
        var onePathThree = $('.modules-content > ul > .one > a > .modules-content__background .one-path-3');
        if (Modernizr.mq('(min-width: 992px)')) {
            $(one).removeAttr('viewBox');
            $(oneFilter).removeAttr('width');
            $(oneTextureOne).removeAttr('d');
            $(oneTextureTwo).removeAttr('d');
            $(onePathOne).removeAttr('d');
            $(onePathTwo).removeAttr('d');
            $(onePathThree).removeAttr('d');
            $(onePathOne).removeAttr('transform');
            $(onePathTwo).removeAttr('transform');
            $(onePathThree).removeAttr('transform');
            $(one).each(function () { $(this)[0].setAttribute('viewBox', '0 0 669.406 194.625')});
            $(oneFilter).each(function () { $(this)[0].setAttribute('width', '655')});
            $(oneTextureOne).each(function () { $(this)[0].setAttribute('d', 'M667,9.74l-4.3,2.34L1.35,2.27,0,0Z')});
            $(oneTextureTwo).each(function () { $(this)[0].setAttribute('d', 'M39.7,15.12,627.24,0l39.15,190.55L0,179.76Z')});
            $(onePathOne).each(function () { $(this)[0].setAttribute('d', 'M667,9.74l-4.3,2.34L1.35,2.27,0,0Z')});
            $(onePathTwo).each(function () { $(this)[0].setAttribute('d', 'M39.7,15.12,627.24,0l39.15,190.55L0,179.76Z')});
            $(onePathThree).each(function () { $(this)[0].setAttribute('d', 'M45.37,7.53,618.95,0,648,169.9,0,159.59Z')});
            $(onePathOne).each(function () { $(this)[0].setAttribute('transform', 'translate(-1,180)')});
            $(onePathTwo).each(function () { $(this)[0].setAttribute('transform', 'translate(0,0)')});
            $(onePathThree).each(function () { $(this)[0].setAttribute('transform', 'translate(5,13)')});
        }
        else if (Modernizr.mq('(max-width: 991px)')) {
            $(one).removeAttr('viewBox');
            $(oneFilter).removeAttr('width');
            $(oneTextureOne).removeAttr('d');
            $(oneTextureTwo).removeAttr('d');
            $(onePathOne).removeAttr('d');
            $(onePathTwo).removeAttr('d');
            $(onePathThree).removeAttr('d');
            $(onePathOne).removeAttr('transform');
            $(onePathTwo).removeAttr('transform');
            $(onePathThree).removeAttr('transform');
            $(one).each(function () { $(this)[0].setAttribute('viewBox', '0 0 291.406 194.625')});
            $(oneFilter).each(function () { $(this)[0].setAttribute('width', '277')});
            $(oneTextureOne).each(function () { $(this)[0].setAttribute('d', 'M799.234,913.718l-1.868,2.335-287.5-9.8-0.588-2.275Z')});
            $(oneTextureTwo).each(function () { $(this)[0].setAttribute('d', 'M529.071,729.077l251.874-2.53,10.173,176.9-277-9.312Z')});
            $(onePathOne).each(function () { $(this)[0].setAttribute('d', 'M526.55,739.425L783.465,724.3l17.119,190.545L509.192,904.059Z')});
            $(onePathTwo).each(function () { $(this)[0].setAttribute('d', 'M526.55,726.557l256.915-5.124,17.119,190.545L509.192,901.191Z')});
            $(onePathThree).each(function () { $(this)[0].setAttribute('d', 'M529.071,729.077l251.874-2.53,10.173,176.9-277-9.312Z')});
            $(onePathOne).each(function () { $(this)[0].setAttribute('transform', 'translate(-509.188 -721.438)')});
            $(onePathTwo).each(function () { $(this)[0].setAttribute('transform', 'translate(-509.188 -721.438)')});
            $(onePathThree).each(function () { $(this)[0].setAttribute('transform', 'translate(-509.188 -721.438)')});
        }
    }

    modulesContent();

    if (isMobile.any) {
        $('body').addClass('is-mobile');

        $('.dropdown-submenu', '#main-nav').on('hide.bs.dropdown', function () {
            return false;
        });
    }

    $(".board table, .main-text table").each(function() {
        var $table = $(this);
        
        if ($table.parent('.table-responsive').length === 0) {
            $table.wrap('<div class="table-responsive"></div>');
        }
    });

    $(".hero__slider").owlCarousel({
        singleItem: true,
        autoPlay: 1e3 * settings.duration,
        slideSpeed: 1e3 * settings.animationDuration,
        paginationSpeed: 1e3 * settings.animationDuration,
        transitionStyle: settings.transition,
        pagination: false,
        mouseDrag: false,
        touchDrag: false
    });

    function modulesContentLinks() {
        $('.modules-content .module').not('#mod_contact').not('#modForum').each(function() {
            if ($(this).find('a').length > 0) {
                var href = $(this).find('a').attr('href');
                $(this).css({'cursor':'pointer'});
                $(this).on('click', function() {
                    window.location.href = href;
                });
            }
        });
    }

    function appendText() {
        var textForum = $('<span>Podziel się z nami <br>swoimi opiniami</span>');
        $(textForum).prependTo('.aside #mod_forum .aside__content');
    }

    appendText();

    modulesContentLinks();

    function fancyboxInit() {
        var a;
        $("a.fancybox").fancybox({
            overlayOpacity: .9,
            overlayColor: settings.overlayColor,
            titlePosition: "outside",
            titleFromAlt: !0,
            titleFormat: function (a, b, c, d) {
                return '<span id="fancybox-title-over">' + texts.image + " " + (c + 1) + " / " + b.length + "</span>" + (a.length ? " &nbsp; " + a.replace(texts.enlargeImage + ": ", "") : "")
            },
            onStart: function (b, c, d) {
                a = b[c];
            },
            onComplete: function () {
                
            },
            onClosed: function () {
            }
        });
    }

    fancyboxInit();

    if (popup.show)
    {
        $.fancybox(
                popup.content,
                {
                    overlayOpacity: .9,
                    overlayColor: settings.overlayColor,
                    padding: 20,
                    autoDimensions: !1,
                    width: popup.width,
                    height: popup.height,
                    transitionIn: "fade",
                    transitionOut: "fade",
                    onStart: function (a) {
                        $("#fancybox-outer").css({
                            background: popup.popupBackground
                        });
                        $("#fancybox-content").addClass("main-text no-margin");
                    }
                }
        );
    }

    $('#gotoTop').children('a').click(function (e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, 1000, 'easeInOutCubic');
    });

    $('#searchForm').on('click', 'button', function (e) {
        if (Modernizr.mq('(max-width: 991px)')) {
            e.stopPropagation();

            if (!$(e.delegateTarget).hasClass('show')) {
                e.preventDefault();
                $(e.delegateTarget).addClass('show');
                $(e.delegateTarget).find('input[type=text]').focus();
                $('body').one('click', function (e) {
                    $('#searchForm').removeClass('show');
                });
            }
        }
    });

    $('#searchForm').on('click', 'input', function (e) {
        e.stopPropagation();
    });
      
    var current_breakpoint = getCurrentBreakpoint();

    $(window).on('resize', function(e) {
        
        var _cb = getCurrentBreakpoint();
        
        if (current_breakpoint !== _cb) {
            current_breakpoint = _cb;
            updateUI();
            resizeModule();
            moduleHeightAlign();
        }

        modulesContent();
        
    });


    function updateUI() {
        if (Modernizr.mq('(max-width: 991px)') && !isMobileView) {
            isMobileView = true;
            $('.header-address', '#banner').prependTo('.header-name', '#banner');
            $('.header-image-1').prependTo('.header-address', '#banner');
            $('.header-image-2').prependTo('.header-welcome .message');
            $('#searchbar').insertAfter('#fonts');
            // $('#menu-top').insertBefore('#toolbar');
            $('#content').prependTo('#content-mobile');
            $('#sidebar-menu').prependTo('#sidebar-menu-mobile');
            $('#modules-top2').prependTo('#modules-top2-mobile');
            $('#modules-bottom').prependTo('#modules-bottom-mobile');
        } 
        else if (Modernizr.mq('(min-width: 992px)') && isMobileView) {
            isMobileView = false;
            $('.header-address', '#banner').insertAfter('.header-name', '#banner');
            $('.header-image-1').insertAfter('.header-address', '#banner');
            $('.header-image-2').insertAfter('.header-address', '#banner');
            $('#searchbar').prependTo('#searchbar-desktop');
            // $('#menu-top').insertAfter('#toolbar');
            $('#content').prependTo('#content-desktop');
            $('#sidebar-menu').prependTo('#sidebar-menu-desktop');
            $('#modules-top2').prependTo('#modules-top2-desktop');
            $('#modules-bottom').prependTo('#modules-bottom-desktop');
            var navHeight = $('.nav').outerHeight(true);
            var modulesHeight = $('.modules-top__list').outerHeight(true);
            var move = navHeight + modulesHeight;
            $('.aside__holder').css({'bottom':move});
        }
        if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 991px)') && !isTabletView) {
            isTabletView = true;
            equalizeHeightsWithPadding('.aside__modules .aside__module');
        }
    }

    function hasSelectedLink() {
        if ($('.menus').find('.selected').length > 0) {
            $('.selected').parents('li').children('a').addClass('has-selected');
        }

        var parent = $('.aside__menu a.selected').parents('li').children('ul').show();
    }

    hasSelectedLink();

    $('body').on('click', '#main-nav a[aria-expanded="true"]', function(e) {
        window.location.href = $(this).attr('href');
    });

    $('#main-nav').on('click', '.dropdown-menu > a', function (e) {

        var $this = $(this);
        var $submenu = $this.next('.dropdown-menu');

        if ($submenu.length > 0) {
            if (isMobile.any) {

                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }

                $this.parent().addClass('open');

            } 
            else {
                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }
            }
        }

    });

    $('.menus', '#sidebar-menu').on('click', 'a', function (e) {

        var $this = $(this);
        var $sublist = $this.next('.dropdown-menu');

        if ($sublist.is(':hidden')) {
            e.preventDefault();
            $sublist.show();
        }

        equalizeHeights('.aside, .main');

    });
    
});

$(function() {
    var a = new Date,
        b = new Date;
    $(document).on("focusin", function(c) {
        $(".keyboard-outline").removeClass("keyboard-outline");
        var d = b < a;
        d && $(c.target).addClass("keyboard-outline");
    }), $(document).on("click", function() {
        b = new Date;
    }), $(document).on("keydown", function() {
        a = new Date;
    });
});

function resizeModule() {
        $('.module.module-common').each(function() {
            var $this = $(this);
            var $parent = $this.parent();
            
            $this.find('.module-body').outerHeight('auto');

            var height = Math.round($this.find('.module-body').outerHeight());
            var heightOverflow = height % 20;
            var heightComplement = 20 - heightOverflow;

            if (heightOverflow) {
                $this.find('.module-body').height(height+heightComplement);
            }
        });
}

function getCurrentBreakpoint() {
    
    var breakpoints = [0, 768, 992, 1200].reverse();
    
    for (var i=0; i < breakpoints.length; i++) {
        if (Modernizr.mq('(min-width: ' + breakpoints[i] + 'px)')) {
            return i;
        }
    }
}

$(window).on('load', function() {
    var navHeight = $('.nav').outerHeight(true);
    var modulesHeight = $('.modules-top__list').outerHeight(true);
    var move = navHeight + modulesHeight;
    $('.aside__holder').css({'bottom':move});
});