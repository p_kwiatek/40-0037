<?php
include_once ( CMS_TEMPL . DS . 'i18n' . DS . $lang . '.php');
include_once ( CMS_TEMPL . DS . 'header.php');
include_once ( CMS_TEMPL . DS . 'top.php');
include_once ( CMS_TEMPL . DS . 'modules_top.php');
?>
<section>
    <div class="container">
        <div class="row desktop">
            <img src="<?php echo $templateDir . '/images/decorations/cloud1.png' ?>" alt="" class="cloud cloud--top1">
            <img src="<?php echo $templateDir . '/images/decorations/cloud2.png' ?>" alt="" class="cloud cloud--top2">
            <img src="<?php echo $templateDir . '/images/decorations/tree-top.png' ?>" alt="" class="tree tree--top">
            <aside class="col-xs-12 col-md-3 aside">
                <?php include_once ( CMS_TEMPL . DS . 'left.php'); ?>
            </aside>
            <section class="col-xs-12 col-md-9 main">
                <div id="modules-top2-desktop">
                    <?php include_once ( CMS_TEMPL . DS . 'modules_top2.php'); ?>
                </div>
                <div id="content-desktop">
                    <div id="content">
                        <?php
                            if ($_GET['c'] == '') {
                                include_once( CMS_TEMPL . DS . 'topAdv.php');
                            }
                            
                            $crumbpathSep = '<i class="icon-right-open-big icon" aria-hidden="true"></i>';
                        ?>

                        <a id="content" tabindex="-1"></a>
                        
                        <div id="crumbpath"><span class="here"><?php echo __('you are here'); ?>:</span><ol class="list-unstyled list-inline"><?php echo show_crumbpath($crumbpath, $crumbpathSep); ?></ol></div>

                        <div id="content_txt">
                            <?php include_once ( $TEMPL_PATH ); ?>
                        </div>

                    </div>
                </div>
                <div id="modules-bottom-desktop">
                    <?php include_once('modules_bottom.php'); ?>
                </div>
            </section>
        </div>
    </div>
</section>
<?php include_once ( CMS_TEMPL . DS . 'footer.php'); ?>
