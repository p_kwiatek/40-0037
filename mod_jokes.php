<h2><?php echo $pageName; ?></h2>
<?php
if ($showMessage)
{
?>
<div class="txt_com"><p><?php echo __('no jokes info'); ?></p></div>
<?php
}
?>

<?php
if ($showList)
{
?>
<ul id="jokes">
    <?php
    foreach ($outRows as $row)
    {
    ?>
    <li>
	<div class="jokeText"><?php echo $row['text']?></div>
	<div class="jokeNick"><?php echo __('added by'); ?>: <strong><?php echo $row['nick']?></strong>, <span class="jokeDate"><?php echo __('day added'); ?>: <?php echo substr($row['date_add'], 0, 10)?></span></div>
    </li>
    <?php
    //strip_tags(str_replace(array("\n", "\r"), "", $_POST['text'])),
    }
    ?>
</ul>
<?php
$url = $PHP_SELF.'?c=' . $_GET['c'] . '&amp;mod=mod_jokes&amp;s=';
include (CMS_TEMPL . DS . 'pagination.php');	
}
?>

<?php
if ($showAddForm)
{
?>
<a id="dodaj" tabindex="-1" class="anchor"></a>
<form  name="formAddJoke" id="formAddJoke" class="" method="post" action="humor,dodaj-zart#dodaj">
    <div class="form">
		<h3 class="subHead"><?php echo __('add joke'); ?></h3>
	    <?php
	    echo $message;
	    ?>    		
		<div class="group">
		    <label for="nick"><?php echo __('nick'); ?>:</label>
		    <input type="text" id="nick" name="nick" class="inText inLong" size="35" maxlength="50" value="<?php echo $nick?>"/><span id="nickMsg" class="msgMarg"></span>
		</div>
		<div class="group">
		    <label for="text"><?php echo __('content'); ?>:</label>
		    <textarea id="text" name="text" rows="8" cols="40" class="inTextArea inLong"><?php echo $text?></textarea><span id="textMsg" class="msgMarg"></span>
		</div>
		<div class="group">
			<p><?php echo __('math info'); ?></p>
		</div>
		<div class="group"></div>
		<div class="group">
		    <label for="topicCaptcha"><?php echo $captchaTxt; ?> <?php echo __('is'); ?></label>
			<input type="text" id="topicCaptcha" name="captcha" size="2" maxlength="2" class="inTextSmall" /><span id="topicCaptchaMsg" class="msgMarg"></span>
		</div>
		<div class="group">
		    <button type="submit" name="ok" class="btnForm"><?php echo __('add'); ?></button>
		</div>
	</div>
</form>
<?php
}
?>
<script type="text/javascript">
    $(document).ready(function() {
		var form = $('#formAddJoke');
		form.submit(function() {
		    if (validateNick() && validateText() && validateCaptcha()){
			//return true;
		    } else {
		       return false;
		    }
		});
		
		$('#nick').blur(validateNick);
		function validateNick(){
		    var value = $('#nick').val();
		    if (value == ''){
				$('#nick').addClass('inError');
				$('#nickMsg').addClass('msgError').text('<?php echo __('error nick'); ?>');
			return false;
		    } else {
				$('#nick').removeClass('inError');
				$('#nickMsg').removeClass('msgError').text('');
			return true;
		    }
		}
		
		$('#text').blur(validateText);
		function validateText(){
		    var value = $('#text').val();
		    if (value == ''){
				$('#text').addClass('inError');
				$('#textMsg').addClass('msgError').text('<?php echo __('error joke'); ?>');
			return false;
		    } else {
				$('#text').removeClass('inError');
				$('#textMsg').removeClass('msgError').text('');
			return true;
		    }
		}

		$('#topicCaptcha').blur(validateCaptcha);
		function validateCaptcha(){
		    var value = $('#topicCaptcha').val();
		    if (value == ''){
				$('#topicCaptcha').addClass('inError');
				$('#topicCaptchaMsg').addClass('msgError').text('<?php echo __('error captcha'); ?>');
			return false;
		    } else {
				$('#topicCaptcha').removeClass('inError');
				$('#topicCaptchaMsg').removeClass('msgError').text('');
			return true;
		    }
		}
    });	
</script>