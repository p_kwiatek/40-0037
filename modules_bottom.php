<?php if ($numRowBottomModules > 0): ?>
<section id="modules-bottom" class="modules-content">
    <ul>
    <?php
        $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';
        
        $links = array();
        
        for ($i = 0; $i < $numRowBottomModules; $i++) {
            $tmp = get_module($outRowBottomModules[$i]['mod_name']);
            
            preg_match($href, $tmp, $m);

            if ($m[2] != '') {
                $links[] = $m[2];
            } else {
                $links[] = trans_url_name($outRowBottomModules[$i]['name']);
            }
        }
        
        $modules_color2 = array(
            'mod_forum',
        );
        
        $module_grid_classes = array(1 => "col-sm-12", 2 => "col-sm-6", 3 => "col-sm-4");
        $module_grid_class = array_key_exists($numRowBottomModules, $module_grid_classes) ? $module_grid_classes[$numRowBottomModules] : $module_grid_classes[3];
    ?>
    <?php for ($i = 0; $i < $numRowBottomModules; $i++): ?>
        <li class="module <?php echo in_array($outRowBottomModules[$i]['mod_name'], $modules_color2) ? 'color2' : ''; ?>" id="<?php echo $outRowBottomModules[$i]['mod_name']; ?>">
            <a href="<?php echo $links[$i]; ?>">
            <?php if ($numRowBottomModules == 3): ?>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="modules-content__background" viewBox="0 0 291.406 194.625">
                    <defs>
                        <filter id="<?php echo 'module-Bottom-filter-' . ($i + 1); ?>" x="514.125" y="726.563" width="277" height="176.875" filterUnits="userSpaceOnUse">
                            <feGaussianBlur result="blur" stdDeviation="2.236" in="SourceAlpha"/>
                            <feFlood result="flood"/>
                            <feComposite result="composite" operator="out" in2="blur"/>
                            <feOffset result="offset"/>
                            <feComposite result="composite-2" operator="in" in2="SourceAlpha"/>
                            <feBlend result="blend" in2="SourceGraphic"/>
                        </filter>
                        <pattern id="<?php echo 'mc-image-' . $outRowBottomModules[$i]['mod_name']; ?>" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                            <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/modules/mc-image-' . $outRowBottomModules[$i]['mod_name'] . '.jpg' ?>"></image>
                        </pattern>
                    </defs>
                    <path class="mc-texture-1" d="M799.234,913.718l-1.868,2.335-287.5-9.8-0.588-2.275Z" transform="translate(-509.188 -721.438)" opacity="0.5" />
                    <path class="mc-texture-2" d="M529.071,729.077l251.874-2.53,10.173,176.9-277-9.312Z" transform="translate(-509.188 -721.438)" opacity="0.5" filter="<?php echo 'url(#module-Bottom-filter-' . ($i + 1) . ')' ?>" />
                    <g>
                        <clipPath id="<?php echo 'mc-bottom-texture-1-' . ($i + 1); ?>">
                            <path d="M526.55,739.425L783.465,724.3l17.119,190.545L509.192,904.059Z" transform="translate(-509.188 -721.438)"></path>
                        </clipPath>
                        <clipPath id="<?php echo 'mc-bottom-texture-2-' . ($i + 1); ?>">
                            <path d="M526.55,726.557l256.915-5.124,17.119,190.545L509.192,901.191Z" transform="translate(-509.188 -721.438)"></path>
                        </clipPath>
                    </g>
                    <image clip-path="<?php echo 'url(#mc-bottom-texture-1-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg'; ?>"></image>
                    <image clip-path="<?php echo 'url(#mc-bottom-texture-2-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg'; ?>"></image>
                    <path d="M529.071,729.077l251.874-2.53,10.173,176.9-277-9.312Z" transform="translate(-509.188 -721.438)" fill="<?php echo 'url(#mc-image-' . $outRowBottomModules[$i]['mod_name'] . ')' ?>" filter="<?php echo 'url(#module-Bottom-filter-' . ($i + 1) . ')' ?>"></path>
                </svg>
            <?php elseif ($numRowBottomModules == 2): ?>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="modules-content__background" viewBox="0 0 291.406 194.625">
                    <defs>
                        <filter id="<?php echo 'module-Bottom-filter-' . ($i + 1); ?>" x="514.125" y="726.563" width="277" height="176.875" filterUnits="userSpaceOnUse">
                            <feGaussianBlur result="blur" stdDeviation="2.236" in="SourceAlpha"/>
                            <feFlood result="flood"/>
                            <feComposite result="composite" operator="out" in2="blur"/>
                            <feOffset result="offset"/>
                            <feComposite result="composite-2" operator="in" in2="SourceAlpha"/>
                            <feBlend result="blend" in2="SourceGraphic"/>
                        </filter>
                        <pattern id="<?php echo 'mc-image-' . $outRowBottomModules[$i]['mod_name']; ?>" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                            <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/modules/mc-image-' . $outRowBottomModules[$i]['mod_name'] . '.jpg' ?>"></image>
                        </pattern>
                    </defs>
                    <path class="mc-texture-1" d="M799.234,913.718l-1.868,2.335-287.5-9.8-0.588-2.275Z" transform="translate(-509.188 -721.438)" opacity="0.5" />
                    <path class="mc-texture-2" d="M529.071,729.077l251.874-2.53,10.173,176.9-277-9.312Z" transform="translate(-509.188 -721.438)" opacity="0.5" filter="<?php echo 'url(#module-Bottom-filter-' . ($i + 1) . ')' ?>" />
                    <g>
                        <clipPath id="<?php echo 'mc-bottom-texture-1-' . ($i + 1); ?>">
                            <path d="M526.55,739.425L783.465,724.3l17.119,190.545L509.192,904.059Z" transform="translate(-509.188 -721.438)"></path>
                        </clipPath>
                        <clipPath id="<?php echo 'mc-bottom-texture-2-' . ($i + 1); ?>">
                            <path d="M526.55,726.557l256.915-5.124,17.119,190.545L509.192,901.191Z" transform="translate(-509.188 -721.438)"></path>
                        </clipPath>
                    </g>
                    <image clip-path="<?php echo 'url(#mc-bottom-texture-1-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg'; ?>"></image>
                    <image clip-path="<?php echo 'url(#mc-bottom-texture-2-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg'; ?>"></image>
                    <path d="M529.071,729.077l251.874-2.53,10.173,176.9-277-9.312Z" transform="translate(-509.188 -721.438)" fill="<?php echo 'url(#mc-image-' . $outRowBottomModules[$i]['mod_name'] . ')' ?>" filter="<?php echo 'url(#module-Bottom-filter-' . ($i + 1) . ')' ?>"></path>
                </svg>
            <?php elseif ($numRowBottomModules == 1): ?>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="modules-content__background" viewBox="0 0 669.406 194.625">
                    <defs>
                        <filter id="module-bottom-filter-1" x="514.125" y="726.563" width="655" height="176.875" filterUnits="userSpaceOnUse">
                            <feGaussianBlur result="blur" stdDeviation="2.236" in="SourceAlpha"/>
                            <feFlood result="flood"/>
                            <feComposite result="composite" operator="out" in2="blur"/>
                            <feOffset result="offset"/>
                            <feComposite result="composite-2" operator="in" in2="SourceAlpha"/>
                            <feBlend result="blend" in2="SourceGraphic"/>
                        </filter>
                        <pattern id="<?php echo 'mc-image-' . $outRowBottomModules[$i]['mod_name']; ?>" preserveAspectRatio="xMinYMin slice" width="100%" height="100%" x="0" y="0">
                            <image height="100%" width="100%" preserveAspectRatio="xMinYMin slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/modules/mc-image-' . $outRowBottomModules[$i]['mod_name'] . '.jpg' ?>"></image>
                        </pattern>
                    </defs>
                    <path class="mc-texture-1" d="M667,9.74l-4.3,2.34L1.35,2.27,0,0Z" transform="translate(-509.188 -721.438)" opacity="0.5" />
                    <path class="mc-texture-2" d="M39.7,15.12,627.24,0l39.15,190.55L0,179.76Z" transform="translate(-509.188 -721.438)" opacity="0.5" filter="<?php echo 'url(#module-bottom-filter-' . ($i + 1) . ')' ?>" />
                    <g>
                        <clipPath id="<?php echo 'mc-bottom-texture-1-' . ($i + 1); ?>">
                            <path d="M667,9.74l-4.3,2.34L1.35,2.27,0,0Z" transform="translate(-1,180)" class="one-path-1"></path>
                        </clipPath>
                        <clipPath id="<?php echo 'mc-bottom-texture-2-' . ($i + 1); ?>">
                            <path d="M39.7,15.12,627.24,0l39.15,190.55L0,179.76Z" class="one-path-2"></path>
                        </clipPath>
                    </g>
                    <image clip-path="<?php echo 'url(#mc-bottom-texture-1-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg'; ?>"></image>
                    <image clip-path="<?php echo 'url(#mc-bottom-texture-2-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg'; ?>"></image>
                    <path d="M45.37,7.53,618.95,0,648,169.9,0,159.59Z" transform="translate(5,13)" class="one-path-3" fill="<?php echo 'url(#mc-image-' . $outRowBottomModules[$i]['mod_name'] . ')' ?>"></path>
                </svg>
            <?php endif ?>
                <h2>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 243 49" preserveAspectRatio="none" class="modules-content__background--title">
                        <path d="M524,513l4,47,233-5,6-44Z" transform="translate(-524 -511)"></path>
                        <g>
                            <clipPath id="mc-title-texture">
                                <path d="M524,513l4,47,233-5,6-44Z" transform="translate(-524 -511)"></path>
                            </clipPath>
                        </g>
                        <image clip-path="url(#mc-title-texture)" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture.jpg'; ?>" opacity="0.5"></image>
                    </svg>
                    <span><?php echo $outRowBottomModules[$i]['name'] ?></span>
                </h2>
            </a>
        </li>
        <?php endfor; ?>
    </ul>
</section>
<?php endif; ?>
