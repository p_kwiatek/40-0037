<footer class="footer">
	<p><?php echo __('designed') ?>: <a href="http://szkolnastrona.pl">Szkolnastrona.pl</a></p>
    <img src="<?php echo $templateDir . '/images/decorations/footer.png' ?>" alt="" class="footer__background">
</footer>
<?php
/*
 * Pobranie z zewnątrz
 */
echo get_url_content($external_text['wwwStart'], 'wwwStart', true);
?>
<a href="#top" id="goTop" class="footer--gotop">NA GÓRĘ</a>
</body>
</html>