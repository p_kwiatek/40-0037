<body>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 164 36" preserveAspectRatio="none" class="base base--button__background">
    <path d="M256,1622l-5,31,162,3,2-36Z" transform="translate(-251 -1620)" id="base-button-background" />
</svg>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 164 36" preserveAspectRatio="none" class="base base--button__shadow">
    <path d="M256,1625l-5,31,162,3,2-36Z" transform="translate(-251 -1623)" opacity="0.5" id="base-button-shadow" />
</svg>
<a id="top"></a>    
<ul class="skip-links list-unstyled">
    <li><a href="#main-menu"><?php echo __('skip to main menu')?></a></li> 
    <li><a href="#add-menu"><?php echo __('skip to additional menu')?></a></li> 
    <li><a href="#content"><?php echo __('skip to content')?></a></li>
    <li><a href="#search"><?php echo __('skip to search')?></a></li>
    <li><a href="mapa_strony"><?php echo __('sitemap')?></a></li>
</ul>

<?php
    include_once ( CMS_TEMPL . DS . 'toolbar.php');
?>

<div id="popup"></div>
<header class="hero">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="hero__title"><?php echo $pageInfo['name']; ?></h1>
                <div id="hero-holder" class="hero__holder">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="hero__dot hero__dot--right" width="10" height="9" viewBox="0 0 10 9">
                        <image width="10" height="9" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAICAMAAAAcEyWHAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAApVBMVEUAAAAAAADV1tbAwcKmp6p9foEAAAAAAADT1NVqa24AAAB+f4FlZmgAAAAAAABJSktMTE4AAAAAAAAAAABAQUJsbW9EREULCwwAAAAAAAAAAAAAAAAAAAAXFxcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADi4+TLzM6usLOSlJe7vL68vcCwsbSbnaCFhomUlpmVl5qOj5KAgYRtbnBqa21eX2EAAADgO+9UAAAAJnRSTlMABFmLiSgBCchfA1vREAhi0R4HN7T66GsaAhlPf6eLaTEKFCs4Hz03kgwAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAAAB3RJTUUH4AwOCwQgrOghPQAAAFJJREFUCNcVy8UBg0AABMC94O5yQHB3S/+tQT7zGwDkw7AcD0AQ264fJAJZGad5WVUNurHtx3mZFmzH9e6fH4SgUZx80ywv3lpWdd4U/wxK6OsD6+oGFDr+ieAAAAAASUVORK5CYII="/>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="hero__dot hero__dot--left" width="10" height="9" viewBox="0 0 10 9">
                        <image width="10" height="9" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAICAMAAAAcEyWHAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAApVBMVEUAAAAAAADV1tbAwcKmp6p9foEAAAAAAADT1NVqa24AAAB+f4FlZmgAAAAAAABJSktMTE4AAAAAAAAAAABAQUJsbW9EREULCwwAAAAAAAAAAAAAAAAAAAAXFxcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADi4+TLzM6usLOSlJe7vL68vcCwsbSbnaCFhomUlpmVl5qOj5KAgYRtbnBqa21eX2EAAADgO+9UAAAAJnRSTlMABFmLiSgBCchfA1vREAhi0R4HN7T66GsaAhlPf6eLaTEKFCs4Hz03kgwAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAAAB3RJTUUH4AwOCwQgrOghPQAAAFJJREFUCNcVy8UBg0AABMC94O5yQHB3S/+tQT7zGwDkw7AcD0AQ264fJAJZGad5WVUNurHtx3mZFmzH9e6fH4SgUZx80ywv3lpWdd4U/wxK6OsD6+oGFDr+ieAAAAAASUVORK5CYII="/>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMaxYMax meet" class="hero__background--outer">
                        <path d="M531.166,73.809L1081.24,41.428,1117.89,449.4,494,426.3Z" transform="translate(-494 -41.438)"></path>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 623.88 407.969" class="hero__background--inner">
                        <path d="M531.166,73.809L1081.24,41.428,1117.89,449.4,494,426.3Z" transform="translate(-494 -41.438)"></path>
                        <g>
                            <clipPath id="hero-background-inner">
                                <path d="M531.166,73.809L1081.24,41.428,1117.89,449.4,494,426.3Z" transform="translate(-494 -41.438)"></path>
                            </clipPath>
                        </g>
                        <image clip-path="url(#hero-background-inner)" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark-long.jpg'; ?>" opacity="0.5"></image>
                    </svg>
                    <?php if ($outBannerTopRows == 0): ?>
                        <?php
                            $img = $templateDir . '/' . 'images' . '/' . 'banner-default.jpg';
                        ?>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 593.099 378.75" class="hero__banner">
                            <defs>
                                <filter id="hero-banner-default-filter">
                                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                                    <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                    <feComposite operator="out" in="floodOut" in2="blurOut" result="compOut" />
                                    <feComposite operator="in" in="compOut" in2="SourceAlpha" />
                                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                    <feBlend mode="normal" in2="SourceGraphic" />
                                </filter>
                                <pattern id="hero-banner-default-pattern" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                                    <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img ?>"></image>
                                </pattern>
                            </defs>
                            <path d="M536.562,79.2L1075.84,52.379l21.78,378.751L504.539,411.192Z" transform="translate(-504.531 -52.375)" fill="url(#hero-banner-default-pattern)" filter="url(#hero-banner-default-filter)"></path>
                        </svg>
                    <?php elseif ($outBannerTopRows == 1): ?>
                        <?php
                            $value = $outBannerTop[0];
                            $img = 'files' . '/' . $lang . '/' . $value['photo'];
                        ?>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 593.099 378.75" class="hero__banner">
                            <defs>
                                <filter id="hero-banner-single-filter">
                                    <feOffset in="SourceAlpha" dx="0" dy="0" />
                                    <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                                    <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                    <feComposite operator="out" in="floodOut" in2="blurOut" result="compOut" />
                                    <feComposite operator="in" in="compOut" in2="SourceAlpha" />
                                    <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                    <feBlend mode="normal" in2="SourceGraphic" />
                                </filter>
                                <pattern id="hero-banner-single-pattern" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                                    <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img ?>"></image>
                                </pattern>
                            </defs>
                            <path d="M536.562,79.2L1075.84,52.379l21.78,378.751L504.539,411.192Z" transform="translate(-504.531 -52.375)" fill="url(#hero-banner-single-pattern)" filter="url(#hero-banner-single-filter)"></path>
                        </svg>
                    <?php else: ?>
                        <div class="hero__slider">
                            <?php 
                                $heroSlide = 0;
                            ?>
                            <?php foreach ($outBannerTop as $value): ?>
                                <?php
                                    $heroSlide++;
                                    $img = 'files' . '/' . $lang . '/' . $value['photo'];
                                ?>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 593.099 378.75" class="hero__banner">
                                    <defs>
                                        <filter id="<?php echo 'hero-banner-filter-' . $heroSlide; ?>">
                                            <feOffset in="SourceAlpha" dx="0" dy="0" />
                                            <feGaussianBlur result="blurOut" stdDeviation="2.236" />
                                            <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                                            <feComposite operator="out" in="floodOut" in2="blurOut" result="compOut" />
                                            <feComposite operator="in" in="compOut" in2="SourceAlpha" />
                                            <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                                            <feBlend mode="normal" in2="SourceGraphic" />
                                        </filter>
                                        <pattern id="<?php echo 'hero-banner-pattern-' . $heroSlide; ?>" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                                            <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img ?>"></image>
                                        </pattern>
                                    </defs>
                                    <path d="M536.562,79.2L1075.84,52.379l21.78,378.751L504.539,411.192Z" transform="translate(-504.531 -52.375)" fill="<?php echo 'url(#hero-banner-pattern-' . $heroSlide . ')'; ?>" filter="<?php echo 'url(#hero-banner-filter-' . $heroSlide . ')'; ?>"></path>
                                </svg>
                            <?php endforeach ?>
                        </div>
                    <?php endif ?>
                </div>
                <?php
                    $pageInfo['name'] = str_replace('w ', 'w&nbsp;', $pageInfo['name']);
                    $pageInfo['name'] = str_replace('im ', 'im&nbsp;', $pageInfo['name']);
                ?>
                <div class="hero__info">
                    <?php
                        echo $headerAddress;
                        if ($pageInfo['email'] != '') {
                            echo '<a href="mailto:' . $pageInfo['email'] . '">' . $pageInfo['email'] . '</a>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</header>

<nav class="nav" id="main-nav">
    <div>
        <img src="<?php echo $templateDir . '/images/nav/leaf.svg'; ?>" alt="" class="nav__decoration--leaf">
        <svg xmlns="http://www.w3.org/2000/svg" width="891.94" height="68" viewBox="0 0 891.94 68" class="nav__background" preserveAspectRatio="none">
            <path d="M494.012,428.991l876.958,21,14.96,47.02L512,497Z" transform="translate(-494 -429)"></path>
            <g>
                <clipPath id="nav-inner">
                    <path d="M494.012,428.991l876.958,21,14.96,47.02L512,497Z" transform="translate(-494 -429)"></path>
                </clipPath>
            </g>
            <image clip-path="url(#nav-inner)" height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-long.jpg'; ?>" opacity="0.5"></image>
        </svg>
        <a id="main-menu" tabindex="-1"></a>
        <?php get_menu_tree('tm', 0, 0, '', false, '', false, false, false, false, true); ?>
    </div>
</nav>

<div class="page-content page-content-mobile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="content-mobile"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="sidebar-menu-mobile" class="aside__holder"></div>
            </div>
        </div>
    </div>
</div>

