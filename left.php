<div class="aside__holder">
    <div id="modules-top2-mobile"></div>
    
    <div id="sidebar-menu-desktop">
        <div id="sidebar-menu" class="aside__menu">
            <a id="add-menu" class="anchor" tabindex="-1"></a>
            <?php get_menu_tree('mg', 0, 0, '', false, '', false, true, true); ?>
            <?php
            foreach ($menuType as $dynMenu) {
                if ($dynMenu['menutype'] != 'mg' && $dynMenu['menutype'] != 'tm') {
                    if ($dynMenu['active'] == 1) {

                        $menuClass = '';
                        if (strlen($dynMenu['name']) > 30) {
                            $menuClass = ' module-name-3';
                        } else if (strlen($dynMenu['name']) > 17 && strlen($dynMenu['name']) <= 30) {
                            $menuClass = ' module-name-2';
                        } else {
                            $menuClass = ' module-name-1';
                        }
                        ?>
                        <h2>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 243 49" preserveAspectRatio="none" class="modules-top__background--title">
                                <path d="M524,513l4,47,233-5,6-44Z" transform="translate(-524 -511)"></path>
                                <g>
                                    <clipPath id="aside-title-texture-<?php echo $dynMenu['menutype'] ?>">
                                        <path d="M524,513l4,47,233-5,6-44Z" transform="translate(-524 -511)"></path>
                                    </clipPath>
                                </g>
                                <image clip-path="<?php echo 'url(#aside-title-texture-' . $dynMenu['menutype'] . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture.jpg'; ?>" opacity="0.5"></image>
                            </svg>
                            <span><?php echo $dynMenu['name'] ?></span>
                        </h2>
                        <?php get_menu_tree($dynMenu['menutype'], 0, 0, '', false, '', false, true, true); ?>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </div>
    <div class="aside__modules">
    <?php
    foreach ($outRowLeftModules as $module) {
        
        $modules_with_icons = array(
            'mod_calendar',
            'mod_stats',
            'mod_kzk',
            'mod_location',
            'mod_contact',
            'mod_forum',
            'mod_timetable',
            'mod_menu',
            'mod_newsletter',
            'mod_gallery',
            'mod_video',
            'mod_jokes',
        );
        
        $modules_color2 = array(
            'mod_forum',
        );
        
        $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';

        $link = '';

        $tmp = get_module($module['mod_name']);

        preg_match($href, $tmp, $m);

        if ($m[2] != '') {
            $link = $m[2];
        } else {
            $link = trans_url_name($module['name']);
        }
        
	?>
        <div class="aside__module" id="<?php echo $module['mod_name']; ?>">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 263 184" preserveAspectRatio="none" class="aside__background--shadow">
                <path d="M231.012,1015.99H493.967l-0.038,184.02L231,1191.61Z" transform="translate(-231 -1016)" />
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" width="263" height="184" viewBox="0 0 263 184" class="aside__background--module aside__background--light" preserveAspectRatio="none">
                <path d="M231.012,1015.99H493.967l-0.038,184.02L231,1191.61Z" transform="translate(-231 -1016)" />
                <g>
                    <clipPath id="<?php echo 'aside-texture-light-' . $module['mod_name']; ?>">
                        <path d="M231.012,1015.99H493.967l-0.038,184.02L231,1191.61Z" transform="translate(-231 -1016)"></path>
                    </clipPath>
                </g>
                <image clip-path="<?php echo 'url(#aside-texture-light-' . $module['mod_name'] . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture.jpg'; ?>" opacity="0.5"></image>
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" width="263" height="184" viewBox="0 0 263 184" class="aside__background--module aside__background--dark" preserveAspectRatio="none">
                <path d="M231.012,1015.99H493.967l-0.038,184.02L231,1191.61Z" transform="translate(-231 -1016)" />
                <g>
                    <clipPath id="<?php echo 'aside-texture-dark-' . $module['mod_name']; ?>">
                        <path d="M231.012,1015.99H493.967l-0.038,184.02L231,1191.61Z" transform="translate(-231 -1016)"></path>
                    </clipPath>
                </g>
                <image clip-path="<?php echo 'url(#aside-texture-dark-' . $module['mod_name'] . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg'; ?>" opacity="0.5"></image>
            </svg>
            <h2>
                <span><?php echo $module['name']?></span>
            </h2>
            <div>
                <div class="aside__content"><?php echo get_module($module['mod_name'])?></div>
            </div>
        </div>
    <?php
        }
    ?>
    </div>
    
    <div id="modules-bottom-mobile"></div>
    
    <?php
    if (count($leftAdv) > 0)
    {
	?>
	<div id="advertsLeftWrapper">
	    <div id="advertsLeftContent">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 263 184" preserveAspectRatio="none" class="aside__background--shadow">
                <path d="M231.012,1015.99H493.967l-0.038,184.02L231,1191.61Z" transform="translate(-231 -1016)" />
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" width="263" height="184" viewBox="0 0 263 184" class="aside__background--module aside__background--light" preserveAspectRatio="none">
                <path d="M231.012,1015.99H493.967l-0.038,184.02L231,1191.61Z" transform="translate(-231 -1016)" />
                <g>
                    <clipPath id="aside-texture-light-advertleft">
                        <path d="M231.012,1015.99H493.967l-0.038,184.02L231,1191.61Z" transform="translate(-231 -1016)"></path>
                    </clipPath>
                </g>
                <image clip-path="url(#aside-texture-light-advertleft)" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture.jpg'; ?>" opacity="0.5"></image>
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" width="263" height="184" viewBox="0 0 263 184" class="aside__background--module aside__background--dark" preserveAspectRatio="none">
                <path d="M231.012,1015.99H493.967l-0.038,184.02L231,1191.61Z" transform="translate(-231 -1016)" />
                <g>
                    <clipPath id="aside-texture-light-dark-advertleft">
                        <path d="M231.012,1015.99H493.967l-0.038,184.02L231,1191.61Z" transform="translate(-231 -1016)"></path>
                    </clipPath>
                </g>
                <image clip-path="url(#aside-texture-light-dark-advertleft)" height="100%" width="100%" preserveAspectRatio="xMinYMin slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg'; ?>" opacity="0.5"></image>
            </svg>
    <?php
    $n = 0;
    foreach ($leftAdv as $adv)
    {
        ?>
        <div class="advertLeft">
            <div class="advertLeft__holder">
        	    <?php
        	    $extUrl = '';
        	    if ($adv['ext_url'] != '')
        	    {
        		$newWindow = '';
        		if ($adv['new_window'] == 1)
        		{
        		    $newWindow = ' target="_blank" ';
        		}
        	    			
        		$swfLink = '';
        		$swfSize = '';
        		if ($adv['attrib'] != '' && $adv['type'] == 'flash')
        		{
        		    //$swfLink = ' style="width:'.$swfSize[0].'px; height:'.$swfSize[1].'px; z-index:1; display:block; position:absolute; left:0; top:0" ';
        		}
        					
        		$extUrl = '<a href="'.$adv['ext_url'].'"'.$newWindow.$swfLink.'>';
        		$extUrlEnd = '</a>';
        	    }
        				
        	    switch ($adv['type'])
        	    {
        		case 'flash':
        		    $swfSize = explode(',', $adv['attrib']);
        		    //echo $extUrl;
        		    ?>
        		    <div id="advLeft_<?php echo $n?>"><?php echo $adv['attrib']?></div>
        		    <script type="text/javascript">
                                swfobject.embedSWF("<?php echo $adv['content'] . '?noc=' . time()?>", "advLeft_<?php echo $n?>", "<?php echo $swfSize[0]?>", "<?php echo $swfSize[1]?>", "9.0.0", "expressInstall.swf", {}, {wmode:"transparent"}, {});
        		    </script>
        		    <?php			
        		    //echo $extUrlEnd;
        		    break;
        					
        		case 'html':
        		    echo $adv['content'];
        		    break;
        					
        		case 'image':
        		default:
        		    $width = '';
        		    if ($adv['content'] != '')
        		    {                    
                                $size = getimagesize($adv['content']);
                                if ($size[0] > $templateConfig['maxWidthLeftAdv'])
                                {
                                    $width = ' width="'.$templateConfig['maxWidthLeftAdv'].'" ';
                                } 
                                echo $extUrl;
                                ?>
                                <img src="<?php echo $adv['content']?>" alt="<?php echo $adv['name']?>" <?php echo $width?>/>
                                <?php
                                echo $extUrlEnd;
        		    }
        		    break;
        	    }
        	    ?>
	       </div>
        </div>
	    <?php
	    $n++;
	}
	?>
	    </div>
	    
	</div>
        
	<?php
    }
    ?>

</div>
