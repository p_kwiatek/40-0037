<div class="main-text">
<h2><?php echo $pageName; ?></h2>
<?php
// Wypisanie artykulow
if ($numArticles > 0)
{	
    $i = 0;
    ?>
    <div class="article-wrapper">
    <h2 class="sr-only"><?php echo __('articles')?></h2>
    <?php
    foreach ($outArticles as $row)
    {
	$highlight = $url = $target = $url_title = $protect = '';
			
	if ($row['protected'] == 1)
	{
	    $protect = '<i class="icon-protected icon"></i>';
	    $url_title = ' title="' . __('page requires login') . '"';
	}				
			
	if (trim($row['ext_url']) != '')
	{
	    if ($row['new_window'] == '1')
	    {
		$target = ' target="_blank"';
	    }	
	    $url_title = ' title="' . __('opens in new window') . '"';
	    $url = ref_replace($row['ext_url']);					
	} else
	{
	    if ($row['url_name'] != '')
	    {
		$url = 'a,' . $row['id_art'] . ',' . $row['url_name'];
	    } else
	    {
		$url = 'index.php?c=article&amp;id=' . $row['id_art'];
	    }
	}	
									
	$margin = ' no-photo';
	if (is_array($photoLead[$row['id_art']]))
	{
	    $margin = '';
	}			
			
	$row['show_date'] = substr($row['show_date'], 0, 10);
        
        $highlight = '';
        if ($row['highlight'] == 1)
        {
$highlight = ' article--highlighted';
        }       
        ?>
        <article class="article<?php echo $highlight?><?php if (!is_array($photoLead[$row['id_art']])): ?> no-photo<?php endif; ?>" id="<?php echo 'article-' . ($i + 1); ?>">
            <?php
                if (is_array($photoLead[$row['id_art']]))
                {
                    $photo = $photoLead[$row['id_art']];
            ?>
            <a href="files/<?php echo $lang?>/<?php echo $photo['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']; ?>" class="article__image fancybox" data-fancybox-group="gallery">
                <span class="sr-only"><?php echo $row['name']; ?></span>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="360" height="248" viewBox="0 0 360 248">
                    <defs>
                        <filter id="<?php echo 'article-filter-' . ($i + 1); ?>" x="534" y="1056" width="345" height="227" filterUnits="userSpaceOnUse">
                            <feGaussianBlur result="blur" stdDeviation="2.236" in="SourceAlpha"/>
                            <feFlood result="flood"/>
                            <feComposite result="composite" operator="out" in2="blur"/>
                            <feOffset result="offset"/>
                            <feComposite result="composite-2" operator="in" in2="SourceAlpha"/>
                            <feBlend result="blend" in2="SourceGraphic"/>
                        </filter>
                        <pattern id="<?php echo 'article-image-' . ($i + 1); ?>" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                            <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $photo['file']?>"></image>
                        </pattern>
                    </defs>
                    <path class="article__image--path" d="M879.957,1289.72l-1.869,2.33-343.224,3.2-0.588-2.27Z" transform="translate(-524 -1047)" opacity="0.5" />
                    <g>
                        <clipPath id="<?php echo 'article-texture-1-' . ($i + 1); ?>">
                            <path d="M525,1051l357,7-2,232-346,3Z" transform="translate(-524 -1047)"></path>
                        </clipPath>
                        <clipPath id="<?php echo 'article-texture-2-' . ($i + 1); ?>">
                            <path d="M525,1051l357,7-2,232-346,3Z" transform="translate(-524 -1047)"></path>
                        </clipPath>
                    </g>
                    <image clip-path="<?php echo 'url(#article-texture-1-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg' ?>"></image>
                    <image clip-path="<?php echo 'url(#article-texture-2-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg' ?>"></image>
                    <path d="M534,1057l345-1-12,220-328,7Z" transform="translate(-524 -1042)" fill="<?php echo 'url(#article-image-' . ($i + 1) . ')' ?>" filter="<?php echo 'url(#article-filter-' . ($i + 1) . ')' ?>"></path>
                </svg>
            </a>
            <?php 
                }
            ?>
            <div class="article__content">
                <?php if ($row['show_date'] != '' && $row['show_date'] != '0000-00-00') { ?>
                    <div class="article__date">
                        <span><?php echo $row['show_date'] ?></span>
                    </div>
                <?php } ?>
                <h4>
                    <a href="<?php echo $url?>" <?php echo $url_title . $target?>>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="7.562" height="10.03" viewBox="0 0 7.562 10.03">
                            <defs>
                                <filter id="<?php echo 'article-title-' . ($i + 1); ?>" x="923.938" y="1814.97" width="7.562" height="10.03" filterUnits="userSpaceOnUse">
                                    <feOffset result="offset" dy="1" in="SourceAlpha"/>
                                    <feGaussianBlur result="blur"/>
                                    <feFlood result="flood" flood-color="#3f9617"/>
                                    <feComposite result="composite" operator="in" in2="blur"/>
                                    <feBlend result="blend" in="SourceGraphic"/>
                                </filter>
                            </defs>
                            <path class="article-title" filter="<?php echo 'url(#article-title-' . ($i + 1) . ')' ?>" d="M923.968,1823.95l4.487-4.48-4.518-4.49h3.052l4.518,4.49-4.487,4.48h-3.052Z" transform="translate(-923.938 -1814.97)"/>
                        </svg>
                        <?php echo $row['name'] . $protect?>
                    </a>
                </h4>
                <?php
                    if (! check_html_text($row['author'], '') )
                    {
                    ?>
                    <div class="authorName"><?php echo __('author'); ?>: <span><?php echo $row['author']?></span></div>
                    <?php
                    }
                ?>
                <p>
                    <?php echo truncate_html($row['lead_text'], 300, '...')?>
                </p>
                <a href="<?php echo $url ?>" <?php echo $url_title . $target ?> class="article__more">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="7.562" height="9" viewBox="0 0 7.562 9">
                            <path d="M899.968,1671.95l4.487-4.48-4.518-4.49h3.052l4.518,4.49-4.487,4.48h-3.052Z" transform="translate(-899.938 -1662.97)"/>
                        </svg>
                    </div><?php echo __('read more') ?>
                    <span class="sr-only"><?php echo __('about')?>: <?php echo $row['name'] . $protect?></span>
                </a>
            </div>
        </article>
    <?php
        $i++;
    }
    ?>
    </div>
<?php
}
?>
</div>
