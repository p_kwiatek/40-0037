<div class="main-text">
<h2><?php echo $pageName?></h2>
<?php
echo $message;

if ($showLoginForm)
{
    include( CMS_TEMPL . DS . 'form_login.php');
}
	
if ($showPage)
{
    echo $row['text'];
		
    if (! check_html_text($row['author'], '') )
    {
	?>
	<div class="authorName"><?php echo __('author'); ?>: <span><?php echo $row['author']?></span></div>
	<?php
    }
			
    /*
     * Articles
     */
    if ($numArticles > 0)
    {	
	$i = 0;
	?>		
	<div class="article-wrapper">
	    <?php
	    foreach ($outRowArticles as $row)
            {
		$highlight = $url = $target = $url_title = $protect = '';
			
		if ($row['protected'] == 1)
		{
		    $protect = '<i class="icon-protected icon" aria-hidden="true"></i>';
		    $url_title = ' title="' . __('page requires login') . '"';
		}				
				
		if (trim($row['ext_url']) != '')
		{
		    if ($row['new_window'] == '1')
		    {
			$target = ' target="_blank"';
		    }	
		    $url_title = ' title="' . __('opens in new window') . '"';
		    $url = ref_replace($row['ext_url']);					
		} else
		{
		    if ($row['url_name'] != '')
		    {
			$url = 'art,' . $row['id_art'] . ',' . $row['url_name'];
		    } else
		    {
			$url = 'index.php?c=article&amp;id=' . $row['id_art'];
		    }
		}	
				
		$margin = ' no-photo';
		if (is_array($photoLead[$row['id_art']]))
		{
		    $margin = '';
		}				
				
		$row['show_date'] = substr($row['show_date'], 0, 10);
		
		$highlight = '';
		if ($row['highlight'] == 1)
		{
		$highlight = ' article--highlighted';
        }       
        ?>
        <article class="article<?php echo $highlight?><?php if (!is_array($photoLead[$row['id_art']])): ?> no-photo<?php endif; ?>" id="<?php echo 'article-' . ($i + 1); ?>">
            <?php
                if (is_array($photoLead[$row['id_art']]))
                {
                    $photo = $photoLead[$row['id_art']];
            ?>
            <a href="files/<?php echo $lang?>/<?php echo $photo['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']; ?>" class="article__image fancybox" data-fancybox-group="gallery">
                <span class="sr-only"><?php echo $row['name']; ?></span>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="360" height="248" viewBox="0 0 360 248">
                    <defs>
                        <filter id="<?php echo 'article-filter-' . ($i + 1); ?>" x="534" y="1056" width="345" height="227" filterUnits="userSpaceOnUse">
                            <feGaussianBlur result="blur" stdDeviation="2.236" in="SourceAlpha"/>
                            <feFlood result="flood"/>
                            <feComposite result="composite" operator="out" in2="blur"/>
                            <feOffset result="offset"/>
                            <feComposite result="composite-2" operator="in" in2="SourceAlpha"/>
                            <feBlend result="blend" in2="SourceGraphic"/>
                        </filter>
                        <pattern id="<?php echo 'article-image-' . ($i + 1); ?>" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                            <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $photo['file']?>"></image>
                        </pattern>
                    </defs>
                    <path d="M879.957,1289.72l-1.869,2.33-343.224,3.2-0.588-2.27Z" transform="translate(-524 -1047)" class="article__image--path" opacity="0.5" />
                    <g>
                        <clipPath id="<?php echo 'article-texture-1-' . ($i + 1); ?>">
                            <path d="M525,1051l357,7-2,232-346,3Z" transform="translate(-524 -1047)"></path>
                        </clipPath>
                        <clipPath id="<?php echo 'article-texture-2-' . ($i + 1); ?>">
                            <path d="M525,1051l357,7-2,232-346,3Z" transform="translate(-524 -1047)"></path>
                        </clipPath>
                    </g>
                    <image clip-path="<?php echo 'url(#article-texture-1-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg' ?>"></image>
                    <image clip-path="<?php echo 'url(#article-texture-2-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg' ?>"></image>
                    <path d="M534,1057l345-1-12,220-328,7Z" transform="translate(-524 -1042)" fill="<?php echo 'url(#article-image-' . ($i + 1) . ')' ?>" filter="<?php echo 'url(#article-filter-' . ($i + 1) . ')' ?>"></path>
                </svg>
            </a>
            <?php 
                }
            ?>
            <div class="article__content">
                <?php if ($row['show_date'] != '' && $row['show_date'] != '0000-00-00') { ?>
                    <div class="article__date">
                        <span><?php echo $row['show_date'] ?></span>
                    </div>
                <?php } ?>
                <h4>
                    <a href="<?php echo $url?>" <?php echo $url_title . $target?>>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="7.562" height="10.03" viewBox="0 0 7.562 10.03">
                            <defs>
                                <filter id="<?php echo 'article-title-' . ($i + 1); ?>" x="923.938" y="1814.97" width="7.562" height="10.03" filterUnits="userSpaceOnUse">
                                    <feOffset result="offset" dy="1" in="SourceAlpha"/>
                                    <feGaussianBlur result="blur"/>
                                    <feFlood result="flood" flood-color="#3f9617"/>
                                    <feComposite result="composite" operator="in" in2="blur"/>
                                    <feBlend result="blend" in="SourceGraphic"/>
                                </filter>
                            </defs>
                            <path class="article-title" filter="<?php echo 'url(#article-title-' . ($i + 1) . ')' ?>" d="M923.968,1823.95l4.487-4.48-4.518-4.49h3.052l4.518,4.49-4.487,4.48h-3.052Z" transform="translate(-923.938 -1814.97)"/>
                        </svg>
                        <?php echo $row['name'] . $protect?>
                    </a>
                </h4>
                <?php
                if (! check_html_text($row['author'], '') )
                {
                ?>
                <div class="authorName"><?php echo __('author'); ?>: <span><?php echo $row['author']?></span></div>
                <?php
                }
                ?>
                <p>
                    <?php echo truncate_html($row['lead_text'], 300, '...')?>
                </p>
                <a href="<?php echo $url ?>" <?php echo $url_title . $target ?> class="article__more">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="7.562" height="9" viewBox="0 0 7.562 9">
                            <path d="M899.968,1671.95l4.487-4.48-4.518-4.49h3.052l4.518,4.49-4.487,4.48h-3.052Z" transform="translate(-899.938 -1662.97)"/>
                        </svg>
                    </div><?php echo __('read more') ?>
                    <span class="sr-only"><?php echo __('about')?>: <?php echo $row['name'] . $protect?></span>
                </a>
            </div>
        </article>
            <?php	
                $i++;
        }
        ?>
            
        <?php

    $url = $PHP_SELF.'?c=' . $_GET['c'] . '&amp;id=' . $_GET['id'] . '&amp;s=';
    include (CMS_TEMPL . DS . 'pagination.php');
    ?>
    </div>
    <?php
    }		
				
        /*
	 * Wypisanie plikow do pobrania
	 */
    if ($numFiles > 0)
    {	
    ?>
	<div class="files-wrapper row">
            <div class="col-xs-12">
                <h3 class="files-header"><?php echo __('files')?></h3>
                <ul class="list-unstyled">
                <?php
                foreach ($outRowFiles as $row)
                {
                    $target = 'target="_blank" ';

                    if (filesize('download/'.$row['file']) > 5000000)
                    {
                        $url = 'download/'.$row['file'];
                    } else
                    {
                        $url = 'index.php?c=getfile&amp;id='.$row['id_file'];
                    }
                    if (trim($row['name']) == '')
                    {
                        $name = $row['file'];
                    } else
                    {
                        $name = $row['name'];
                    }

                    $size = file_size('download/'.$row['file']);	
                    ?>		
                    <li>
                        <a href="<?php echo $url?>" <?php echo $target?>>
                            <i class="icon-doc-text-inv icon" aria-hidden="true"></i>
                            <span class="title">
                                <?php echo $name?>
                                <span class="size">(<?php echo $size?>)</span>
                            </span>                        
                        </a>                        
                    </li>
                    <?php
                }
                ?>
                </ul>
            </div>
	</div>
    <?php
    }
		
    /*
     *  Wypisanie zdjec
     */
    if ($numPhotos > 0)
    {	
	$i = 0;
	?>
	<div class="gallery-wrapper row">
        <div class="col-xs-12">
            <h3 class="gallery-header"><?php echo __('gallery')?></h3>
            <ul class="list-unstyled row gallery">
            <?php
            foreach ($outRowPhotos as $row)
            {
                $i++;
                $noMargin = '';
                if ($i == $pageConfig['zawijaj'])
                {
                    $noMargin = ' noMargin';
                }
                ?> 
                <li class="col-xs-12 col-sm-6 col-md-4">
                    <a href="files/<?php echo $lang?>/<?php echo $row['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']?>" data-fancybox-group="gallery" class="photo fancybox">
                        <span class="sr-only"><?php echo $row['name']; ?></span>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="360" height="248" viewBox="0 0 360 248">
                            <defs>
                                <filter id="<?php echo 'article-filter-' . ($i + 1); ?>" x="534" y="1056" width="345" height="227" filterUnits="userSpaceOnUse">
                                    <feGaussianBlur result="blur" stdDeviation="2.236" in="SourceAlpha"/>
                                    <feFlood result="flood"/>
                                    <feComposite result="composite" operator="out" in2="blur"/>
                                    <feOffset result="offset"/>
                                    <feComposite result="composite-2" operator="in" in2="SourceAlpha"/>
                                    <feBlend result="blend" in2="SourceGraphic"/>
                                </filter>
                                <pattern id="<?php echo 'article-image-' . ($i + 1); ?>" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                                    <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $row['file']?>"></image>
                                </pattern>
                            </defs>
                            <path d="M879.957,1289.72l-1.869,2.33-343.224,3.2-0.588-2.27Z" transform="translate(-524 -1047)" class="article__image--path" opacity="0.5" />
                            <g>
                                <clipPath id="<?php echo 'article-texture-1-' . ($i + 1); ?>">
                                    <path d="M525,1051l357,7-2,232-346,3Z" transform="translate(-524 -1047)"></path>
                                </clipPath>
                                <clipPath id="<?php echo 'article-texture-2-' . ($i + 1); ?>">
                                    <path d="M525,1051l357,7-2,232-346,3Z" transform="translate(-524 -1047)"></path>
                                </clipPath>
                            </g>
                            <image clip-path="<?php echo 'url(#article-texture-1-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg' ?>"></image>
                            <image clip-path="<?php echo 'url(#article-texture-2-' . ($i + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg' ?>"></image>
                            <path d="M534,1057l345-1-12,220-328,7Z" transform="translate(-524 -1042)" fill="<?php echo 'url(#article-image-' . ($i + 1) . ')' ?>" filter="<?php echo 'url(#article-filter-' . ($i + 1) . ')' ?>"></path>
                        </svg>
                        <?php
                        if (! check_html_text($row['name'], '') )
                        {
                            ?>
                            <p class="photo-name" aria-hidden="true"><?php echo $row['name']?></p>
                            <?php
                        }
                        ?>
                    </a>
                </li>
            <?php
            }
            ?>
            </ul>
        </div>
    </div>
	<?php
    }		

    if ($outSettings['pluginTweet'] == 'włącz')
    {
	 echo '<div class="Tweet"><iframe frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html" style="width:80px; height:30px;"></iframe></div>';  
    }

    if ($outSettings['pluginFB'] == 'włącz')
    {
	$fb_url = urlencode('http://'.$pageInfo['host'].'/index.php?c=page&amp&id='. $_GET['id']);
	echo '<div class="FBLike"><iframe src=\'http://www.facebook.com/plugins/like.php?href='.$fb_url.'&amp;layout=standard&amp;show_faces=true&amp;width=400&amp;action=like&amp;font=tahoma&amp;colorscheme=light&amp;height=32&amp;show_faces=false\' scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:32px;"></iframe></div>';   
}
}
?>
</div>