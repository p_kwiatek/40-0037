<?php
if ($showAll)
{
    ?>
<div class="gallery-wrapper row">
    <div class="col-xs-12">
        <h2><?php echo $pageName?></h2>
        <ul class="list-unstyled row gallery">
        <?php
        if (count($albums) > 0)
        {
            $n = 0;
            foreach ($albums as $value)
            {
                $n++;
                ?>
                <li class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <a href="<?php echo $value['link']?>" class="photo">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 360 248.25">
                            <defs>
                                <filter id="<?php echo 'article-filter-' . ($n + 1); ?>" x="534" y="1056" width="345" height="227" filterUnits="userSpaceOnUse">
                                    <feGaussianBlur result="blur" stdDeviation="2.236" in="SourceAlpha"/>
                                    <feFlood result="flood"/>
                                    <feComposite result="composite" operator="out" in2="blur"/>
                                    <feOffset result="offset"/>
                                    <feComposite result="composite-2" operator="in" in2="SourceAlpha"/>
                                    <feBlend result="blend" in2="SourceGraphic"/>
                                </filter>
                            </defs>
                            <pattern id="<?php echo 'article-image-' . ($n + 1); ?>" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                                <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $value['file']?>"></image>
                            </pattern>
                            <path d="M879.957,1289.72l-1.869,2.33-343.224,3.2-0.588-2.27Z" transform="translate(-524 -1047)" class="article__image--path" opacity="0.5" />
                            <g>
                                <clipPath id="<?php echo 'article-texture-1-' . ($n + 1); ?>">
                                    <path d="M525,1051l357,7-2,232-346,3Z" transform="translate(-524 -1047)"></path>
                                </clipPath>
                                <clipPath id="<?php echo 'article-texture-2-' . ($n + 1); ?>">
                                    <path d="M525,1051l357,7-2,232-346,3Z" transform="translate(-524 -1047)"></path>
                                </clipPath>
                            </g>
                            <image clip-path="<?php echo 'url(#article-texture-1-' . ($n + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg' ?>"></image>
                            <image clip-path="<?php echo 'url(#article-texture-2-' . ($n + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg' ?>"></image>
                            <path d="M534,1057l345-1-12,220-328,7Z" transform="translate(-524 -1042)" fill="<?php echo 'url(#article-image-' . ($n + 1) . ')' ?>" filter="<?php echo 'url(#article-filter-' . ($n + 1) . ')' ?>"></path>
                        </svg>
                        <p class="photo-name"><?php echo $value['name']?></p>
                    </a>
                </li>
                <?php
            }	
        } else
        {
            ?>
            <p><?php echo __('no photo album added')?></p>
            <?php
        }
        ?>
    </div>
</div>
    <?php
}
if ($showOne)
{
    ?>
<div class="gallery-wrapper row">
    <div class="col-xs-12">
        <h2><?php echo $pageName?></h2>
        <?php 
        echo $message;
        ?>
	<?php 
	if ($showGallery)
	{
	    if (count($outRows) > 0)
	    {
                ?>
                <ul class="list-unstyled row gallery">
                <?php
		$n = 0;
		foreach ($outRows as $value)
		{
		    $n++;
		    ?>
		    <li class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
			<a href="files/<?php echo $lang?>/<?php echo $value['file']?>" title="<?php echo __('enlarge image') . ': ' . $value['name']?>" data-fancybox-group="gallery" class="photo fancybox">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 360 248.25">
                    <defs>
                        <filter id="<?php echo 'article-filter-' . ($n + 1); ?>" x="534" y="1056" width="345" height="227" filterUnits="userSpaceOnUse">
                            <feGaussianBlur result="blur" stdDeviation="2.236" in="SourceAlpha"/>
                            <feFlood result="flood"/>
                            <feComposite result="composite" operator="out" in2="blur"/>
                            <feOffset result="offset"/>
                            <feComposite result="composite-2" operator="in" in2="SourceAlpha"/>
                            <feBlend result="blend" in2="SourceGraphic"/>
                        </filter>
                        <pattern id="<?php echo 'article-image-' . ($n + 1); ?>" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" x="0" y="0">
                            <image height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="files/<?php echo $lang?>/mini/<?php echo $value['file']?>"></image>
                        </pattern>
                    </defs>
                    <path d="M879.957,1289.72l-1.869,2.33-343.224,3.2-0.588-2.27Z" transform="translate(-524 -1047)" class="article__image--path" opacity="0.5" />
                    <g>
                        <clipPath id="<?php echo 'article-texture-1-' . ($n + 1); ?>">
                            <path d="M525,1051l357,7-2,232-346,3Z" transform="translate(-524 -1047)"></path>
                        </clipPath>
                        <clipPath id="<?php echo 'article-texture-2-' . ($n + 1); ?>">
                            <path d="M525,1051l357,7-2,232-346,3Z" transform="translate(-524 -1047)"></path>
                        </clipPath>
                    </g>
                    <image clip-path="<?php echo 'url(#article-texture-1-' . ($n + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg' ?>"></image>
                    <image clip-path="<?php echo 'url(#article-texture-2-' . ($n + 1) . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark.jpg' ?>"></image>
                    <path d="M534,1057l345-1-12,220-328,7Z" transform="translate(-524 -1042)" fill="<?php echo 'url(#article-image-' . ($n + 1) . ')' ?>" filter="<?php echo 'url(#article-filter-' . ($n + 1) . ')' ?>"></path>
                </svg>
                <?php
                if (! check_html_text($value['name'], '') )
                {
                    ?>
                    <p class="photo-name" aria-hidden="true"><?php echo $value['name']?></p>
                    <?php
                }
                ?>
            </a>
		    </li>
                <?php
		}
                ?>
                </ul>
                <?php
	    }
        }
    ?>
    </div>
</div>
<?php
if ($showLoginForm)
{
    ?>
    <div class="main-text">
    <?php
    include( CMS_TEMPL . DS . 'form_login.php');
    ?>
    </div>
    <?php
}
?>
    
<?php
}
?>
