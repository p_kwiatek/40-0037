<button class="toolbar--hamburger" aria-controls="main-nav" aria-expanded="false" data-target="#main-nav" data-toggle="collapse" type="button">
    <i aria-hidden="true"></i>
    <i aria-hidden="true"></i>
    <i aria-hidden="true"></i>
    <span class="sr-only"><?php echo __('expand')?></span>
    <span class="title sr-only"><?php echo __('main menu')?></span>
</button>
<div id="toolbar" class="toolbar">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1170 48" class="toolbar__background" preserveAspectRatio="none">
        <path d="M229,48L216,0H1386l-13,48H229Z" transform="translate(-216)"></path>
        <g>
            <clipPath id="toolbar-texture">
                <path d="M229,48L216,0H1386l-13,48H229Z" transform="translate(-216)"></path>
            </clipPath>
        </g>
        <image clip-path="url(#toolbar-texture)" height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/toolbar/toolbar-texture.jpg'; ?>" opacity="0.5"></image>
    </svg>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="toolbar__holder toolbar__holder--left">
                    <ul class="toolbar__links">
                        <li>
                            <a href="index.php" title="<?php echo __("home page"); ?>">
                                <img src="<?php echo $pathTemplate; ?>/images/toolbar/toolbar-home.svg" width="17" height="16" alt="" /><?php echo __("home page"); ?>
                            </a>
                        </li>
                        <li><a href="mapa_strony" title="<?php echo __('sitemap'); ?>"><img src="<?php echo $pathTemplate; ?>/images/toolbar/toolbar-sitemap.svg" width="14" height="20" alt="<?php echo __("sitemap"); ?>" /><?php echo __('sitemap') ?></a></li>
                        <li><a href="<?php echo $pageInfo['bip'] ?>" title="<?php echo __('bip'); ?>"><img src="<?php echo $pathTemplate; ?>/images/toolbar/toolbar-bip.svg" width="54" height="26" alt="<?php echo __("bip"); ?>" /></a></li>
                    </ul>
                </div>
                <div class="toolbar__holder toolbar__holder--right">
                    <div class="toolbar__fonts">
                        <span><?php echo __('font size'); ?>:</span>
                        <ul>
                            <li><a href="ch_style.php?style=0">A<span class="sr-only"><?php echo __('font default')?></span></a></li>
                            <li><a href="ch_style.php?style=r1">A<sup>+</sup><span class="sr-only"><?php echo __('font bigger')?></span></a></li>
                            <li><a href="ch_style.php?style=r2">A<sup>++</sup><span class="sr-only"><?php echo __('font biggest')?></span></a></li>
                        </ul>
                    </div>
                    <?php
                        if ($_SESSION['contr'] == 0)
                        {
                            $set_contrast = 1;
                            $contrast_txt = __('contrast version');
                        } else {
                            $set_contrast = 0;
                            $contrast_txt = __('graphic version');
                        }
                    ?>
                    <a href="ch_style.php?contr=<?php echo $set_contrast ?>" class="toolbar__contrast"><div></div><span class="sr-only"><?php echo $contrast_txt?></span></a>
                    <div class="toolbar__search">
                        <a id="search" tabindex="-1"></a>
                        <span class="sr-only"><?php echo __('search')?></span>
                        <form id="searchForm" name="f_szukaj" method="get" action="index.php">
                            <input name="c" type="hidden" value="search" />
                            <label for="kword"><span class="sr-only"><?php echo __('search query')?></span></label>
                            <input type="text" id="kword" name="kword" value="<?php echo __('search query'); ?>" onfocus="if (this.value=='<?php echo __('search query'); ?>') {this.value=''};" onblur="if (this.value=='') {this.value='<?php echo __('search query'); ?>'};"/>
                            <button type="submit" name="search" class="btnSearch">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="82" height="39" viewBox="0 0 82 39" preserveAspectRatio="none">
                                    <path class="toolbar__search--path" d="M1285,9l-5,30,77,3,2-35Z" transform="translate(-1279 -7)"></path>
                                    <g>
                                        <clipPath id="toolbar-button-texture">
                                            <path d="M1285,9l-5,30,77,3,2-35Z" transform="translate(-1279 -7)"></path>
                                        </clipPath>
                                    </g>
                                    <image clip-path="url(#toolbar-button-texture)" height="100%" width="100%" preserveAspectRatio="xMidYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-light.png'; ?>"></image>
                                </svg>
                                <span><?php echo __('search action'); ?></span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
