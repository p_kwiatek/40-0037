<?php if (count($outRowTopModules) > 0): ?>
<section class="modules-top">
    <div class="modules-top__holder">
        <svg class="modules-top__background" xmlns="http://www.w3.org/2000/svg" width="883" height="210" viewBox="0 0 883 210" preserveAspectRatio="none">
            <path d="M503.012,495.991l882.958,1-25.04,187.02L516,705.607Z" transform="translate(-503 -496)"></path>
            <g>
                <clipPath id="modules-top-background">
                    <path d="M503.012,495.991l882.958,1-25.04,187.02L516,705.607Z" transform="translate(-503 -496)"></path>
                </clipPath>
            </g>
            <image clip-path="url(#modules-top-background)" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture-dark-long.jpg'; ?>" opacity="0.5"></image>
        </svg>
        <ul class="modules-top__list">
            <?php
            $i = 0;
            foreach ($outRowTopModules as $module):
                $i++;
                $mod = 'module' . $i;
            ?>
            <li id="<?php echo $module['mod_name']; ?>" class="module <?php echo $mod; ?>">
                <div>
                    <h2>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 243 49" preserveAspectRatio="none" class="modules-top__background--title">
                            <path d="M524,513l4,47,233-5,6-44Z" transform="translate(-524 -511)"></path>
                            <g>
                                <clipPath id="mt-title-texture-<?php echo $i ?>">
                                    <path d="M524,513l4,47,233-5,6-44Z" transform="translate(-524 -511)"></path>
                                </clipPath>
                            </g>
                            <image clip-path="<?php echo 'url(#mt-title-texture-' . $i . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture.jpg'; ?>" opacity="0.5"></image>
                        </svg>
                        <span><?php echo $module['name']; ?></span>
                    </h2>
                    <div class="modules-top__content">
                        <?php echo get_module($module['mod_name'], 'top'); ?>
                    </div>
                </div>
            </li>
            <?php      
                endforeach;
            ?>
        </ul>
    </div>
</section>
<?php endif; ?>