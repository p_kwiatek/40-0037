<?php
if ($pagination['end'] > 1)
{
    ?>
<nav class="pagination">
    <?php
    $active_page = $pagination['active'];

    if ( !isset($pagination['active']) )
    {
        $active_page = 1;
    }
    ?>
    <p class="sr-only"><?php echo __('page')?>: <strong><?php echo $active_page?></strong>/<?php echo $pagination['end']?></p>
    <ul>
    <?php
    if ($pagination['start'] != $pagination['prev'])
    {
        ?>
        <li>
            <a href="<?php echo $url . $pagination['start']?>" rel="nofollow" class="pagination__first">
                <svg xmlns="http://www.w3.org/2000/svg" width="38" height="39" viewBox="0 0 38 39">
                    <path d="M530,1965l-5,31,36,3,2-36Z" transform="translate(-525 -1963)"  class="pagination__svg--path" />
                    <g>
                        <clipPath id="pagination-texture-first">
                            <path d="M530,1965l-5,31,36,3,2-36Z" transform="translate(-525 -1963)"></path>
                        </clipPath>
                    </g>
                    <image clip-path="url(#pagination-texture-first)" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture.jpg' ?>" opacity="0.5"></image>
                </svg>
                <span class="sr-only"><?php echo __('first page')?></span>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="18" viewBox="0 0 16 18" class="pagination__sign pagination__sign--long">
                    <defs>
                        <filter id="filter-pagination-first" x="726.938" y="1972.97" width="16" height="18" filterUnits="userSpaceOnUse">
                            <feOffset result="offset" dy="1" in="SourceAlpha"/>
                            <feGaussianBlur result="blur"/>
                            <feFlood result="flood" flood-color="#fff" flood-opacity="0.53"/>
                            <feComposite result="composite" operator="in" in2="blur"/>
                            <feBlend result="blend" in="SourceGraphic"/>
                        </filter>
                    </defs>
                    <path class="pagination__first--path" d="M726.968,1989.95l8.487-8.48-8.518-8.49h3.052l8.518,8.49-8.487,8.48h-3.052Zm4,0,8.487-8.48-8.518-8.49h3.052l8.518,8.49-8.487,8.48h-3.052Z" transform="translate(-726.938 -1972.97)" filter="url(#filter-pagination-first)"/>
                </svg>
            </a>
        </li>
        <li>
            <a href="<?php echo $url . $pagination['prev']?>" rel="nofollow" class="pagination__prev">
                <svg xmlns="http://www.w3.org/2000/svg" width="38" height="39" viewBox="0 0 38 39">
                    <path d="M530,1965l-5,31,36,3,2-36Z" transform="translate(-525 -1963)"  class="pagination__svg--path" />
                    <g>
                        <clipPath id="pagination-texture-prev">
                            <path d="M530,1965l-5,31,36,3,2-36Z" transform="translate(-525 -1963)"></path>
                        </clipPath>
                    </g>
                    <image clip-path="url(#pagination-texture-prev)" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture.jpg' ?>" opacity="0.5"></image>
                </svg>
                <span class="sr-only"><?php echo __('prev page')?></span>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12" height="18" viewBox="0 0 11.563 18" class="pagination__sign pagination__sign--short">
                    <defs>
                        <filter id="filter-pagination-prev" x="538.5" y="1972.97" width="12" height="18" filterUnits="userSpaceOnUse">
                            <feOffset result="offset" dy="1" in="SourceAlpha"/>
                            <feGaussianBlur result="blur"/>
                            <feFlood result="flood" flood-color="#fff" flood-opacity="0.53"/>
                            <feComposite result="composite" operator="in" in2="blur"/>
                            <feBlend result="blend" in="SourceGraphic"/>
                        </filter>
                    </defs>
                    <path class="pagination__prev--path" d="M550.031,1989.95l-8.486-8.48,8.518-8.49h-3.052l-8.518,8.49,8.487,8.48h3.051Z" filter="url(#filter-pagination-prev)" transform="translate(-538.5 -1972.97)"/>
                </svg>
            </a>
        </li>
        <?php
    }		
    foreach ($pagination as $k => $v)
    {
        if (is_numeric($k))
        {
            ?>
        <li>
            <a href="<?php echo $url . $v?>" rel="nofollow" class="pagination__page">
                <svg xmlns="http://www.w3.org/2000/svg" width="38" height="39" viewBox="0 0 38 39">
                    <path d="M639,1965l5,31-36,3-2-36Z" transform="translate(-606 -1963)"  class="pagination__svg--path" />
                    <g>
                        <clipPath id="pagination-texture-<?php echo $v?>">
                            <path d="M639,1965l5,31-36,3-2-36Z" transform="translate(-606 -1963)"></path>
                        </clipPath>
                    </g>
                    <image clip-path="<?php echo 'url(#pagination-texture-' . $v . ')' ?>" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture.jpg' ?>" opacity="0.5"></image>
                </svg>
                <span class="sr-only"><?php echo __('page')?></span>
                <span><?php echo $v?></span>
            </a>
        </li>
            <?php
        } else if ($k == 'active')
        {
            ?>
        <li class="pagination__page--active">
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" width="38" height="39" viewBox="0 0 38 39">
                    <path d="M639,1965l5,31-36,3-2-36Z" transform="translate(-606 -1963)" />
                    <g>
                        <clipPath id="pagination-texture-active">
                            <path d="M639,1965l5,31-36,3-2-36Z" transform="translate(-606 -1963)"></path>
                        </clipPath>
                    </g>
                    <image clip-path="url(#pagination-texture-active)" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture.jpg' ?>" opacity="0.5"></image>
                </svg>
                <span class="sr-only"><?php echo __('page')?></span>
                <span><?php echo $v?></span>
            </div>
        </li>
            <?php
        }			
    }		
    if ($pagination['active'] != $pagination['end'])
    {
        ?>
        <li>
            <a href="<?php echo $url . $pagination['next']?>" rel="nofollow" class="pagination__next">
                <svg xmlns="http://www.w3.org/2000/svg" width="38" height="39" viewBox="0 0 38 39">
                    <path d="M639,1965l5,31-36,3-2-36Z" transform="translate(-606 -1963)"  class="pagination__svg--path" />
                    <g>
                        <clipPath id="pagination-texture-next">
                            <path d="M639,1965l5,31-36,3-2-36Z" transform="translate(-606 -1963)"></path>
                        </clipPath>
                    </g>
                    <image clip-path="url(#pagination-texture-next)" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture.jpg' ?>" opacity="0.5"></image>
                </svg>
                <span class="sr-only"><?php echo __('next page')?></span>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12" height="18" viewBox="0 0 12 18" class="pagination__sign pagination__sign--short">
                    <defs>
                        <filter id="filter-pagination-next" x="688.938" y="1972.97" width="12" height="18" filterUnits="userSpaceOnUse">
                            <feOffset result="offset" dy="1" in="SourceAlpha"/>
                            <feGaussianBlur result="blur"/>
                            <feFlood result="flood" flood-color="#fff" flood-opacity="0.53"/>
                            <feComposite result="composite" operator="in" in2="blur"/>
                            <feBlend result="blend" in="SourceGraphic"/>
                        </filter>
                    </defs>
                    <path class="pagination__next--path" d="M688.968,1989.95l8.487-8.48-8.518-8.49h3.052l8.518,8.49-8.487,8.48h-3.052Z" filter="url(#filter-pagination-next)" transform="translate(-688.938 -1972.97)"/>
                </svg>
            </a>
        </li>
        <li>
            <a href="<?php echo $url . $pagination['end']?>" rel="nofollow" class="pagination__last">
                <svg xmlns="http://www.w3.org/2000/svg" width="38" height="39" viewBox="0 0 38 39">
                    <path d="M639,1965l5,31-36,3-2-36Z" transform="translate(-606 -1963)"  class="pagination__svg--path" />
                    <g>
                        <clipPath id="pagination-texture-last">
                            <path d="M639,1965l5,31-36,3-2-36Z" transform="translate(-606 -1963)"></path>
                        </clipPath>
                    </g>
                    <image clip-path="url(#pagination-texture-last)" height="100%" width="100%" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/textures/texture.jpg' ?>" opacity="0.5"></image>
                </svg>
                <span class="sr-only"><?php echo __('last page')?></span>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="18" viewBox="0 0 16 18" class="pagination__sign pagination__sign--long">
                    <defs>
                        <filter id="filter-pagination-last" x="726.938" y="1972.97" width="16" height="18" filterUnits="userSpaceOnUse">
                            <feOffset result="offset" dy="1" in="SourceAlpha"/>
                            <feGaussianBlur result="blur"/>
                            <feFlood result="flood" flood-color="#fff" flood-opacity="0.53"/>
                            <feComposite result="composite" operator="in" in2="blur"/>
                            <feBlend result="blend" in="SourceGraphic"/>
                        </filter>
                    </defs>
                    <path class="pagination__last--path" d="M726.968,1989.95l8.487-8.48-8.518-8.49h3.052l8.518,8.49-8.487,8.48h-3.052Zm4,0,8.487-8.48-8.518-8.49h3.052l8.518,8.49-8.487,8.48h-3.052Z" transform="translate(-726.938 -1972.97)" filter="#filter-pagination-last" />
                </svg>
            </a>
        </li>
        <?php
    }
    ?>	
    </ul>
</nav>
    <?php
}
?>